//A
//  Demonstration of boson polarization correction in the EWKino pair production signals
//

// System include(s):
#include <memory>
#include <cstdlib>
#include <iostream>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TLegend.h>

#include "xAODRootAccess/Init.h"
#ifdef ROOTCORE
  #include "xAODRootAccess/TEvent.h"
#else
  #include "POOLRootAccess/TEvent.h"
#endif // ROOTCORE
#include "PATInterfaces/CorrectionCode.h"
#include "CPAnalysisExamples/errorcheck.h"

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"

// Local include(s):
#include "TruthDecayContainer/BosonPolReweightingTool.h"
#include "TruthDecayContainer/ProcClassifier.h"

/////////////////////////////////////////////////////////
int main( int argc, char* argv[] ) {

  // The application's name:
  const char* APP_NAME = argv[ 0 ];

  // Unload the command line arguments
  TString inFilePath="";
  int maxEvents=-1;
  
  for(int k=0; k<argc; ++k) {
    TString key = argv[k];
    if(key=="-inputFile")   inFilePath  = argv[k+1];
    if(key=="-maxEvents")   maxEvents   = atoi(argv[k+1]);
  }

  // Check if we received a file name:
  if ( inFilePath=="" ) {
    Error( APP_NAME, "No file name received!" );
    Error( APP_NAME, "  Usage: %s -inputFile [full path to the xAOD file]", APP_NAME );
    Error( APP_NAME, "  Options: -maxEvents [Max number of events to loop over]");
    return 1;
  }

  // Open the input file:
  Info( APP_NAME, "Opening file: %s", inFilePath.Data() );
  std::auto_ptr< TFile > ifile( TFile::Open( inFilePath, "READ" ) );
  CHECK( ifile.get() );

  // Create a TEvent object:
#ifdef ROOTCORE
  xAOD::TEvent event( xAOD::TEvent::kAthenaAccess );
#else
  POOL::TEvent event( POOL::TEvent::kAthenaAccess );
#endif

  CHECK( event.readFrom( ifile.get() ) );
  Info( APP_NAME, "Number of events in the file: %i",
        static_cast< int >( event.getEntries() ) );
  
  // Check if this is simulation
  const xAOD::EventInfo* eventInfo = 0;
  event.getEntry(0);
  CHECK( event.retrieve( eventInfo, "EventInfo") );

  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
    Error( APP_NAME, "This is a data file. Exit. ");
    return 0;
  }

  // Create the tool(s) to test:
  BosonPolReweightingTool *bosonRwgtTool = new BosonPolReweightingTool();
  bosonRwgtTool->init();

  // ------------------------- Loop over the events ------------------------- //

  // Histograms for validation
  TH1F* hist_costhStar      = new TH1F("hist_costhStar_rwgt","Black: nominal / Red: re-weighted; cos#theta^{*} (W/Z; #tilde{#chi}); Entries",20,-1,1);
  TH1F* hist_costhStar_rwgt = new TH1F("hist_costhStar_rwgt","",20,-1,1);

  int nevt = maxEvents>0 ? maxEvents : 2000; 

  for ( int entry = 0; entry < nevt; ++entry ) {

    // Tell the object which entry to look at:
    event.getEntry( entry );

    // Retrieve containers
    const xAOD::TruthParticleContainer* truthParticles = 0;
    CHECK( event.retrieve( truthParticles, "TruthParticles" ));

    if(entry<=10)
      Info(APP_NAME, "////////////////////// Event: %i //////////////////////////////", entry);
    else if(entry%100==0)
      Info(APP_NAME, "---- processed %i events", entry);

    std::vector<EWGDecay> decays = bosonRwgtTool->GetEWGDecays(truthParticles);
    float polWeight = 1.;

    int id=0;
    for(EWGDecay decay : decays){
      id++;

      // Helicity angle of the decay (angle between the boson and the parent EWKino, at the parent rest frame)
      float cosThetaStar = bosonRwgtTool->getCosThetaStar(decay.Xparent, decay.boson);

      // Default weight (nominal EWKino config in the simplified models)
      float weight_winoBino_tanb10_posMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "wino", "bino",  // assuming wino->bino
								       10., 1);         // tan(beta)=10, sign(mu)>0
      //
      // Variations
      //
      
      // Low/High tan(beta).  Supported for tanb=[2,30] & extrapolated outside the window,
      float weight_winoBino_tanb2_posMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "wino", "bino", 2., 1);        
      float weight_winoBino_tanb30_posMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "wino", "bino", 30., 1);        

      // Sign of mu parameter
      float weight_winoBino_tanb10_negMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "wino", "bino", 10, -1);        

      // Higgsino->Bino decay (hinoA(B): lighter(heavier) one if neutral higgsino)
      float weight_hinoABino_tanb10_posMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "hinoA", "bino", 10, 1);        

      // Wino->higgsino decay
      float weight_winoHinoA_tanb10_posMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "wino", "hinoA", 10, 1);        

      // Higgsino->Wino decay
      float weight_hinoAWino_tanb10_posMu = bosonRwgtTool->getPolWeight(decay.Xparent, decay.Xchild, decay.boson, 
								       "hinoA", "wino", 10, 1);        

      // Print out the summary (for the first 10 events)
      if(entry<=10){
	Info(APP_NAME, "decay %i  Xparent_pdgId=%i", id, decay.Xparent->pdgId());
	Info(APP_NAME, "decay %i   Xchild_pdgId=%i", id, decay.Xchild->pdgId());
	Info(APP_NAME, "decay %i    boson_pdgId=%i", id, decay.boson->pdgId());
	Info(APP_NAME, "decay %i   cosThetaStar=%f", id, cosThetaStar);
	Info(APP_NAME, "decay %i   weight_winoBino_tanb10_posMu=%f", id, weight_winoBino_tanb10_posMu);
	Info(APP_NAME, "decay %i    weight_winoBino_tanb2_posMu=%f", id, weight_winoBino_tanb2_posMu);
	Info(APP_NAME, "decay %i   weight_winoBino_tanb30_posMu=%f", id, weight_winoBino_tanb30_posMu);
	Info(APP_NAME, "decay %i   weight_winoBino_tanb10_negMu=%f", id, weight_winoBino_tanb10_negMu);
	Info(APP_NAME, "decay %i  weight_hinoABino_tanb10_posMu=%f", id, weight_hinoABino_tanb10_posMu);
	Info(APP_NAME, "decay %i  weight_winoHinoA_tanb10_posMu=%f", id, weight_winoHinoA_tanb10_posMu);
	Info(APP_NAME, "decay %i  weight_hinoAWino_tanb10_posMu=%f", id, weight_hinoAWino_tanb10_posMu);
      }
      
      // Simply multiplied ignoring the inter-gaugino spin correlation
      polWeight *= weight_winoBino_tanb10_posMu;

      // Fill histograms
      hist_costhStar->Fill(cosThetaStar);
      hist_costhStar_rwgt->Fill(cosThetaStar, weight_winoBino_tanb10_posMu);
    }

    if(entry<=10)
      Info(APP_NAME, "polWeight:  %f", polWeight);

    if(entry==10) Info(APP_NAME, "First 10 events past. Will stop the print-out...");

  }
  
  // Draw validation histogram
  hist_costhStar->SetMinimum(0);
  hist_costhStar->SetMaximum(hist_costhStar_rwgt->GetMaximum()*1.3);

  hist_costhStar->SetLineWidth(2);
  hist_costhStar_rwgt->SetLineWidth(2);

  hist_costhStar->SetLineColor(1);
  hist_costhStar_rwgt->SetLineColor(2);

  hist_costhStar->Draw("h");
  hist_costhStar_rwgt->Draw("h sames");

  gPad->Print("test_polReweight.pdf","pdf");

  return 0;
}
