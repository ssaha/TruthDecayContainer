#ifndef TRUTHEVENT_VV_h
#define TRUTHEVENT_VV_h

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TObject.h"

#include "TruthDecayContainer/Decay_boson.h"

typedef TLorentzVector tlv;


class TruthEvent_VV : public TObject {

 public:

  TruthEvent_VV();

  ~TruthEvent_VV();

  TruthEvent_VV(const TruthEvent_VV &rhs);

  virtual void set_evt(const Decay_boson &in_B1, const Decay_boson &in_B2);
        
  virtual void finalize();
  
  virtual void getFlavLabel_2L(const Decay_boson &w1, const Decay_boson &w2, 
			       int &flavLabel, bool & isSS, bool &isSF, int &ntau, int &nem);
  
  static TString getFlavString_2L(int flavLabel);

  virtual void getFlavLabel_3L(const Decay_boson &w, const Decay_boson &z, 
			       int &flavLabel, bool &isSFOS, bool &isSF, int &ntau, int &nem);
  
  static TString getFlavString_3L(int flavLabel);

  static TString getDecayString(Decay_boson in_B1, Decay_boson in_B2);

  virtual void check_swap();

  virtual void swap12();

  virtual void print();

  virtual void clear();

  ClassDef(TruthEvent_VV,1);

  // tlv 
  Decay_boson B1, B2;
  tlv pMis;
  tlv pBB;

  // decay label
  int  flavLabel_2L;
  bool isSF_2L;
  bool isSS_2L;
  int  flavLabel_3L;
  bool isSFOS_3L; 
  bool isSF_3L; 
  int  nTau; 
  int  nEMu; 
  int  decayLabel; 

 private:  


};







#endif
