#ifndef TruthDecayContainer_DecayHandle_h
#define TruthDecayContainer_DecayHandle_h

// xAOD includes
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"

// CP/ASG tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "AsgTools/AsgMetadataTool.h"

// TruthDecayContainer
#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/Decay_tau.h"
#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/Decay_H.h"
#include "TruthDecayContainer/TruthEvent_Vjets.h"
#include "TruthDecayContainer/TruthEvent_VV.h"
#include "TruthDecayContainer/TruthEvent_VVV.h"
#include "TruthDecayContainer/TruthEvent_TT.h"
#include "TruthDecayContainer/TruthEvent_XX.h"
#include "TruthDecayContainer/TruthEvent_GG.h"

// C++ includes
#include <vector>
#include <iostream>
#include <stdarg.h>

class StatusCode;

typedef TLorentzVector tlv;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  //  Main driver class that systematically retrieves info from truth decay chains and stores in the container classes.
  //  Helper functions are implemented in DecayHandleHelper.cxx
  //
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static SG::AuxElement::ConstAccessor< unsigned int > cacc_TDC_truthOrigin( "classifierParticleOrigin" );
static SG::AuxElement::ConstAccessor< unsigned int > cacc_TDC_truthType  ( "classifierParticleType"   );
static SG::AuxElement::ConstAccessor< int > cacc_TDC_firstEgMotherType( "firstEgMotherType" );
static SG::AuxElement::ConstAccessor< int > cacc_TDC_firstEgMotherOrigin( "firstEgMotherOrigin" );
static SG::AuxElement::ConstAccessor< int > cacc_TDC_firstEgMotherPdgId( "firstEgMotherPdgId" );
static SG::AuxElement::ConstAccessor< int > cacc_TDC_HadronConeExclTruthLabelID( "HadronConeExclTruthLabelID" );
static SG::AuxElement::ConstAccessor< int > cacc_TDC_PartonTruthLabelID( "PartonTruthLabelID" );
static SG::AuxElement::ConstAccessor< int > cacc_TDC_ConeTruthLabelID( "ConeTruthLabelID" );

class DecayHandle 
{

 private:
 
 public:
  DecayHandle();
  ~DecayHandle();

  ///
  /// Initialize all tools needed for this class
  ///
  StatusCode init(bool useGeV);
  StatusCode init(int DSID, bool useGeV, TString generator="");

  //
  // Pass the xAOD containers to the class.
  // Do this first every events before you call the retrival functions ("Get...") below.
  // 
  inline void loadContainers(const xAOD::TruthParticleContainer* truthParticles,
			     const xAOD::TruthParticleContainer* truthElectrons,
			     const xAOD::TruthParticleContainer* truthMuons,
			     const xAOD::TruthParticleContainer* truthTaus,
			     const xAOD::TruthParticleContainer* truthPhotons,
			     const xAOD::TruthParticleContainer* truthNeutrinos,
			     const xAOD::TruthParticleContainer* truthBosonWDP,
			     const xAOD::TruthParticleContainer* truthTopWDP,
			     const xAOD::TruthParticleContainer* truthBSMWDP,
			     const xAOD::JetContainer*           truthJets,
			     TString forcedTruthProc=""){
    m_truthParticles = (xAOD::TruthParticleContainer*) truthParticles;
    m_truthElectrons = (xAOD::TruthParticleContainer*) truthElectrons;
    m_truthMuons     = (xAOD::TruthParticleContainer*) truthMuons;
    m_truthTaus      = (xAOD::TruthParticleContainer*) truthTaus;
    m_truthPhotons   = (xAOD::TruthParticleContainer*) truthPhotons;
    m_truthNeutrinos = (xAOD::TruthParticleContainer*) truthNeutrinos;
    m_truthBosonWDP  = (xAOD::TruthParticleContainer*) truthBosonWDP;
    m_truthTopWDP    = (xAOD::TruthParticleContainer*) truthTopWDP;
    m_truthBSMWDP    = (xAOD::TruthParticleContainer*) truthBSMWDP;
    m_truthJets      = (xAOD::JetContainer*)           truthJets;    

    m_forcedTruthProc = forcedTruthProc;
  }

  //
  // Methods to retrieve additional partons
  //
  StatusCode RetrieveDocumentLines(std::vector<tlv> &addPartons,  std::vector<int> &addPartons_pdg, float ptThreshInGeV=15., bool fillOnlyHF=false);
  //StatusCode RetrieveCharmAndBottom(std::vector<tlv> &addPartons,  std::vector<int> &addPartons_pdg, float ptThreshInGeV=15.);
  StatusCode RetrieveTruthJets(std::vector<tlv> &truthJets,  std::vector<int> &truthJets_label);

  // SM decays (tau/W/Z/H)
  StatusCode GetTauDecay       (xAOD::TruthParticle* mcp_tau, Decay_tau &evt);
  StatusCode GetWDecay_Impl    (const std::vector<xAOD::TruthParticle*> &Wchildren, Decay_boson &evt);
  StatusCode GetWDecay         (xAOD::TruthParticle *mcp_W, Decay_boson &evt);  
  StatusCode GetZDecay_Impl    (const std::vector<xAOD::TruthParticle*> &Zchildren, Decay_boson &evt);
  StatusCode GetZDecay         (xAOD::TruthParticle *mcp_Z, Decay_boson &evt);
  StatusCode GetHDecay         (xAOD::TruthParticle *mcp_H, Decay_H &evt);
  int        GetHiggsDecayLabel();
  
  // SUSY decays 
  xAOD::TruthParticle* GetChildEWG( xAOD::TruthParticle* mcp_susy);
  StatusCode GetEWGDecayViaW ( xAOD::TruthParticle *mcp, Decay_boson &evt);  
  StatusCode GetEWGDecayViaZ ( xAOD::TruthParticle *mcp, Decay_boson &evt);  
  StatusCode GetEWGDecayViaH ( xAOD::TruthParticle *mcp, Decay_H &evt);  
  StatusCode GetEWGDecay     ( xAOD::TruthParticle *mcp_chi, 
			       tlv &pchi_child, int &chi_child_pdg, Decay_boson &boson, tlv &pN1 );
  StatusCode GetEWGDecayGGMZh( xAOD::TruthParticle *mcp_chi, 
			       tlv &pG, int &chi_child_pdg, Decay_boson &boson);
  StatusCode GetGluinoDecay  ( xAOD::TruthParticle *mcp_go,
			       tlv &pq1, int &q1_pdg, tlv &pq2, int &q2_pdg, 
			       tlv &pchi, int & chi_pdg, Decay_boson &boson, tlv &pN1);  

  //   
  // Methods to retrieve full decay chains of pair produced particles 
  //

  // SM Processes
  StatusCode GetDecayChain_Vjets(TruthEvent_Vjets *evt);
  StatusCode GetDecayChain_Vjets_Sherpa(TruthEvent_Vjets *evt);
  StatusCode GetDecayChain_Wjets_Sherpa(TruthEvent_Vjets *evt);
  StatusCode GetDecayChain_Zjets_Sherpa(TruthEvent_Vjets *evt);
  StatusCode GetDecayChain_Znunu_Sherpa(TruthEvent_Vjets *evt);
  StatusCode GetDecayChain_GammaJets_Sherpa(TruthEvent_Vjets *evt);
  StatusCode GetDecayChain_TT(TruthEvent_TT *evt);

  StatusCode GetDecayChain_VV(TruthEvent_VV *evt);
  StatusCode GetDecayChain_VV_FullLep(TruthEvent_VV *evt);           // In case TruthParticleContainer is not available
  StatusCode GetDecayChain_WW(TruthEvent_VV *evt);
  StatusCode GetDecayChain_WW2L2Nu_Sherpa(TruthEvent_VV *evt);    // Inclusive llvv
  StatusCode GetDecayChain_WZ(TruthEvent_VV *evt);
  StatusCode GetDecayChain_WZ3L1Nu_Sherpa(TruthEvent_VV *evt);    // Inclusive lllv
  StatusCode GetDecayChain_WZ1L3Nu_Sherpa(TruthEvent_VV *evt); // Inclusive lvvv
  StatusCode GetDecayChain_ZZ(TruthEvent_VV *evt);
  StatusCode GetDecayChain_ZZ4L_Sherpa(TruthEvent_VV *evt);    // Inclusive llll/llvv/vvvv
  StatusCode GetDecayChain_WH(TruthEvent_VV *evt);
  StatusCode GetDecayChain_ZH(TruthEvent_VV *evt);
  StatusCode GetDecayChain_HH(TruthEvent_VV *evt);

  StatusCode GetDecayChain_VVV(TruthEvent_VVV *evt);
  StatusCode GetDecayChain_VVV_FullLep(TruthEvent_VVV *evt);
  
  // SUSY Processes
  StatusCode GetDecayChain_XX(TruthEvent_XX *evt);           // EW gaugino pair prod

  StatusCode GetDecayChain_GG(TruthEvent_GG *evt);           // Gluino pair prod

  // Get status=1 muons
  StatusCode GetFinalMuons(std::vector<xAOD::TruthParticle*> &particles, 
			   std::vector<int> &parentPdgId, 
			   std::vector<int> &parentBarcode);

  // Get status=1 neutrinos
  StatusCode GetFinalNeutrinos(std::vector<xAOD::TruthParticle*> &particles, 
			       std::vector<int> &parentPdgId, 
			       std::vector<int> &parentBarcode);

  //
  StatusCode GetFakeLeptons();           // For test use

  // Helpers for Sherpa VV(V) samples
  StatusCode GetTaus_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_taus, std::vector<Decay_tau> &v_tauDecays);
  StatusCode GetNonTauElectrons_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_eles, const std::vector<Decay_tau> &v_tauDecays);
  StatusCode GetNonTauMuons_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_mus, const std::vector<Decay_tau> &v_tauDecays);
  StatusCode GetNonTauNeutrlinos_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_nus, const std::vector<Decay_tau> &v_tauDecays);


  //
  // Helper funcitons to handle decays (src@DecayHandleHelpers.cxx)
  //
  static unsigned int getNChildren(xAOD::TruthParticle* mcp);
  static xAOD::TruthParticle* getChild(xAOD::TruthParticle* mcp, int idx);
  static unsigned int getNParents(xAOD::TruthParticle* mcp);
  static xAOD::TruthParticle* getParent(xAOD::TruthParticle* mcp, int idx);

  // Desend/ascend the chain of self-decay.
  // Return the first/last xAOD::TruthParticle* instance for the same particle
  //  e.g. descend(A):   decend until the childrend no longer contain A
  //       descend(A,B): decend until the childrend no longer contain A or particle with pdgId=B
  static xAOD::TruthParticle* descend (xAOD::TruthParticle* mcp, int pdg_ignored=0, bool debug=false);
  static xAOD::TruthParticle* ascend  (xAOD::TruthParticle* mcp, int pdg_ignored=0, bool debug=false);

  // Return the parent particle (ignoring self-decays)
  static xAOD::TruthParticle*  getParent     (xAOD::TruthParticle* mcp);
  static int                   getParentPdgId(xAOD::TruthParticle* mcp);

  // Who are you?
  static bool isChargino(int pdgId);
  static bool isNeutralino(int pdgId);
  static bool isEWG(int pdgId);

  // Return the 4-vector of the particle
  tlv  setTLV(xAOD::TruthParticle* , double massInGeV=-1);
  tlv  setTLV_fin(xAOD::TruthParticle* , double massInGeV=-1);

  // Print the particle info
  void print_mcp(xAOD::TruthParticle* );

  // Return the electric charge of given SM particle
  float get_charge(int pdgId);

  // Return if the lepton matches with the tau children.
  bool isFromTau(xAOD::TruthParticle* mcp, const Decay_tau &tau);

  // Fake/Real classification from IFF
  static bool isPromptRealEle   (xAOD::TruthParticle* mcp);
  static bool isPromptRealMuon  (xAOD::TruthParticle* mcp);
  static bool isPromptRealLep   (xAOD::TruthParticle* mcp);
  static bool isChargeFlip      (xAOD::TruthParticle* mcp);
  static bool isConversion      (xAOD::TruthParticle* mcp);
  static bool isHadronDecay     (xAOD::TruthParticle* mcp);
  static bool isHF_nonPrompt_tau(xAOD::TruthParticle* mcp);
  static bool isHF_nonPrompt_mu (xAOD::TruthParticle* mcp);
  static bool isHF_nonPrompt_B  (xAOD::TruthParticle* mcp);
  static bool isHF_nonPrompt_C  (xAOD::TruthParticle* mcp);
     
  // Log and output messages. Replica of the "MsgLog" class in SusySkimMaker written by Matthew Gignac (UBC).
  static void WARNING(TString className,const char *va_fmt, ...);
  static void ERROR(TString className,const char *va_fmt, ...);  
  static void INFO(TString className,const char *va_fmt, ...);
  static TString format(const char* va_fmt, va_list args);
  static TString formatOutput(TString className,TString msg,TString msgStreamSize);

  // Generator labeling
  inline bool isSherpa(){
    return TString(m_generator).Contains("Sherpa") 
      || TString(m_generator).Contains("sherpa") 
      || TString(m_generator).Contains("SHERPA");
  }
  inline bool isPythia(){
    return TString(m_generator).Contains("Pythia") 
      || TString(m_generator).Contains("pythia") 
      || TString(m_generator).Contains("PYTHIA");
  }

  inline bool isMadGraph(){
    return TString(m_generator).Contains("Madgraph") 
      || TString(m_generator).Contains("madgraph") 
      || TString(m_generator).Contains("MADGRAPH");
  }
  inline bool isPowheg(){
    return TString(m_generator).Contains("Powheg") 
      || TString(m_generator).Contains("powheg") 
      || TString(m_generator).Contains("POWHEG");
  }
   
  //////////////////////////////////////////////////////////////////////////////////////
 
 protected:
  float          m_convertFromMeV;
  int            m_DSID;
  TString        m_generator;

  // CP/ASG tools 
  asg::AsgMetadataTool                   *m_metaDataTool;

  // Truth containers as global variables 
  xAOD::TruthParticleContainer* m_truthParticles;
  xAOD::TruthParticleContainer* m_truthElectrons;
  xAOD::TruthParticleContainer* m_truthMuons;
  xAOD::TruthParticleContainer* m_truthTaus;
  xAOD::TruthParticleContainer* m_truthPhotons;
  xAOD::TruthParticleContainer* m_truthNeutrinos;
  xAOD::TruthParticleContainer* m_truthBosonWDP;
  xAOD::TruthParticleContainer* m_truthTopWDP;
  xAOD::TruthParticleContainer* m_truthBSMWDP;

  xAOD::JetContainer*           m_truthJets;

  //
  TString m_forcedTruthProc;
};

#endif
