#ifndef TruthDecayContainer_BosonPolReweightingTool_h
#define TruthDecayContainer_BosonPolReweightingTool_h

// xAOD includes
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

// ROOT include(s):
#include "TH2.h"
#include "TF1.h"

// CP/ASG tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "AsgTools/AsgMetadataTool.h"

// C++ includes
#include <vector>
#include <iostream>
#include <stdarg.h>

// TruthDecayContainer
#include "TruthDecayContainer/DecayHandle.h"

class StatusCode;

typedef TLorentzVector tlv;

struct EWGDecay{
  xAOD::TruthParticle* Xparent;  
  xAOD::TruthParticle* Xchild;  
  xAOD::TruthParticle* boson;  
};

class BosonPolReweightingTool 
{

 private:
 
 public:
  BosonPolReweightingTool();
  ~BosonPolReweightingTool();

  ///
  /// Initialize all tools needed for this class
  ///
  StatusCode init();

  void loadHistograms(TFile* fin, std::map<TString, TH2F*> &histDict);
  
  // Decay retrieving functions  
  std::vector<EWGDecay> GetEWGDecays(const xAOD::TruthParticleContainer* truthParticles); 
  StatusCode GetEWGDecay(xAOD::TruthParticle* Xparent,
			 xAOD::TruthParticle* &Xchild,
			 xAOD::TruthParticle* &boson);
  
  // CosThetaStar retrieving functions  
  float getCosThetaStar(tlv pXparent, tlv pBoson);
  float getCosThetaStar(xAOD::TruthParticle *Xparent, xAOD::TruthParticle *boson);

  // Utility for retrieving histogram name
  TString getProcString(const int &Xparent_pdg, const int &Xchild_pdg, const int &boson_pdg,
			const TString &XparentFlavor, const TString &XchildFlavor);

  // Utility for retrieving coefficients from histograms
  float fetchCoefficients_fixedTanb(const TString &coeffType, const TString &proc, 
				    const float &mHeavy, const float &mLight, 
				    const int &tanb_i, const int &sgnMu);  
  
  float fetchCoefficients(const TString &coeffType, const TString &proc, 
			  const float &mHeavy, const float &mLight, 
			  const float &tanb, const int &sgnMu);
  
  void normalizeCoefficients(float &F0, float &FL, float &FR);
  
  // Polarization weight evaluation
  float getPolWeight(xAOD::TruthParticle *Xparent, 
		     xAOD::TruthParticle *Xchild, 
		     xAOD::TruthParticle *boson, 
		     TString XparentFlavor="wino", 
                     TString XchildFlavor="bino", 
		     float   tanb=10., 
		     int     sgnMu=1);
  
  // In case you want to go with one line
  float getCombinedPolWeight(xAOD::TruthParticleContainer *truthParticles,
			     TString XparentFlavor="wino", 
			     TString XchildFlavor="bino", 
			     float   tanb=10., 
			     int     sgnMu=1);
  
  
 protected:
  float                    m_convertFromMeV;
  TF1*                     m_func_cosThetaStar;
  std::map<TString,TH2F*>  m_spinMap;
  DecayHandle*             m_decayHandle;

  
};

#endif
