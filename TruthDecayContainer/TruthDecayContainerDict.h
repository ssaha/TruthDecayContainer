#ifndef TRUTHDECAYCONTAINER_MYCLASSESDICT_H
#define TRUTHDECAYCONTAINER_MYCLASSESDICT_H

#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/Decay_tau.h"
#include "TruthDecayContainer/Decay_H.h"

#include "TruthDecayContainer/TruthEvent_Vjets.h"
#include "TruthDecayContainer/TruthEvent_VV.h"
#include "TruthDecayContainer/TruthEvent_TT.h"
#include "TruthDecayContainer/TruthEvent_XX.h"
#include "TruthDecayContainer/TruthEvent_GG.h"

#endif // not TRUTHDECAYCONTAINER_MYCLASSESDICT_H


