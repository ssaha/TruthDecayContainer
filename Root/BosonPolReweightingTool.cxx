#include "TruthDecayContainer/BosonPolReweightingTool.h"

// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):
#include <TError.h>
#include <TString.h>
#include <TFile.h>
#include <TKey.h>
#include <TObject.h>

// EDM include(s):
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

// Other include(s):
#include "AsgTools/StatusCode.h"
#include "AsgTools/AsgMetadataTool.h"
#include "TruthDecayContainer/ProcClassifier.h"
#include "TruthDecayContainer/TruthDecayUtils.h"
#include "PathResolver/PathResolver.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
BosonPolReweightingTool::BosonPolReweightingTool() : 
  m_convertFromMeV(0.001),
  m_func_cosThetaStar(0),
  m_decayHandle(0)
{}
//////////////////////////////////////////////////////////////////////////////////////////////////////
BosonPolReweightingTool::~BosonPolReweightingTool()
{}
//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode BosonPolReweightingTool::init()
{  
  // Initialize DecayHandle instance 
  m_decayHandle = new DecayHandle();
  m_decayHandle->init(true); // useGeV=true

  // Load spin maps
  std::string path = PathResolverFindCalibFile("TruthDecayContainer/bosonSpinReweighting/spinMap.root");
  TFile *fin = new TFile(path.c_str(),"read");
  loadHistograms(fin, m_spinMap);

  // Initialize cosThetaStar shape function form
  m_func_cosThetaStar = new TF1("func_cosThetaStar","[0]*((3/4.)*[1]*(1-x^2) + (3/8.)*[2]*(1-x)^2 + (3/8.)*[3]*(1+x)^2)/( (3/4.)*[1]+(3/8.)*[2]+(3/8.)*[3]   )",-1,1);
    
  // Return gracefully
  std::cout << "BosonPolReweightingTool:init()  INFO  Initialization succeeded." << std::endl;

  return StatusCode::SUCCESS;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////
void BosonPolReweightingTool::loadHistograms(TFile* fin, std::map<TString, TH2F*> &histDict) {
  
  std::vector<TString> objNameList;   
  TList* list = fin->GetListOfKeys() ;
   if (!list) { printf("<E> No keys found in file\n") ; exit(1) ; }
   
   TIter next(list) ;
   TKey* key ;
   TObject* obj;
   
   do{    
     key = (TKey*)next();  if(!key) break;     
     try {
       obj = key->ReadObj() ;     
       if ( TString(obj->IsA()->GetName()) != TString("TH2F")  ) continue;
       TString name = obj->GetName();
       if(!name.EndsWith("intp") ) continue;
       std::cout << "BosonPolReweightingTool::loadHistograms  INFO  Loading " << name << std::endl;

       TH2F* hist = (TH2F*) fin->Get(name);
       histDict.insert(std::pair<TString, TH2F*>(name,hist));

     } catch (std::exception& e)
       {
	 std::cout << e.what() << std::endl;
	 std::cout << "BosonPolReweightingTool::loadHistograms  ERROR  Hit the loading limit of TFile. Do split into small files. Exit." << std::endl;
	 return;
       }       
     
   } while (key) ;
   
   std::cout << "BosonPolReweightingTool::loadHistograms  INFO  Loaded " << histDict.size() << " maps." << std::endl;

   return;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<EWGDecay> BosonPolReweightingTool::GetEWGDecays(const xAOD::TruthParticleContainer* truthParticles){ 

  //
  // Retrive the list of two body decays of C1/C2/N1/N2/N3/N4 appeared in the truth particle container.
  //

  std::vector<EWGDecay> v_decays;

  if(!truthParticles) return v_decays;
  
  // +++++ Find prompt EW gauginos ++++++ //
  for(int idx=0, n=(int)truthParticles->size(); idx<n; idx++) {               
    xAOD::TruthParticle* tp = (xAOD::TruthParticle*)(truthParticles->at(idx)) ;   
    int pdg = tp->pdgId();

    // if C1/C2/N1/N2/N3/N4
    if(m_decayHandle->isEWG(pdg)){
      xAOD::TruthParticle* fin_tp = m_decayHandle->descend(tp,0);
      if(fin_tp->status()==1) continue; // Skip if this is already LSP

      // Check if this already got picked up (if it did -> skip)
      bool isNewDecay=true;
      for(EWGDecay registered_decay : v_decays){
	if(fin_tp == registered_decay.Xparent){ isNewDecay=false; break; }
      }
      if(!isNewDecay) continue;

      // Fill a new EWGDecay 
      EWGDecay decay;
      decay.Xparent = fin_tp;
      if(!GetEWGDecay(fin_tp, decay.Xchild, decay.boson)) continue;
      if(decay.boson->pdgId()==25) continue; // skip higgs (as no reweighting is needed)

      v_decays.push_back(decay);
    }
  }

  return v_decays;   
  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode BosonPolReweightingTool::GetEWGDecay(xAOD::TruthParticle* Xparent, // input
					    xAOD::TruthParticle* &Xchild,  // output
					    xAOD::TruthParticle* &boson    // output
					    ){ 

  Xchild=0;
  boson=0;
  
  // ------- Retrieve child EW gaugino ---- //
  xAOD::TruthParticle* fin_Xparent   = m_decayHandle->descend(Xparent,0);  	        
  Xchild = m_decayHandle->GetChildEWG(fin_Xparent);

  if(!Xchild){
    std::cout << "BosonPolReweightingTool::GetEWGDecay  ERROR  Could not retrieve child gaugino. Exit." << std::endl;
    return StatusCode::FAILURE;
  }
  // ------------

  // Guess the species of associated boson based on the types of parent/child gaugino.
  const bool isParentC = m_decayHandle->isChargino(Xparent->pdgId());
  const bool isParentN = m_decayHandle->isNeutralino(Xparent->pdgId());
  const bool isChildC = m_decayHandle->isChargino(Xchild->pdgId());
  const bool isChildN = m_decayHandle->isNeutralino(Xchild->pdgId());
  const bool isViaW  = (isParentC && isChildN) || (isParentN && isChildC);
  const bool isViaZh = (isParentN && isChildN) || (isParentC && isChildC);

  if(!isViaW && !isViaZh){
    std::cout << "BosonPolReweightingTool::GetEWGDecay  ERROR  Weird gaugino-gauge boson combination. Parent pdg: " << Xparent->pdgId() << ", chidl_pdg: " << Xchild->pdgId() << std::endl;
    std::cout << "BosonPolReweightingTool::GetEWGDecay  ERROR  isParentC: " << isParentC << ", isParentN: " << isParentN << ", isChildC: " << isChildC << ", isChildN: " << isChildN << ". Exit." << std::endl;
    return StatusCode::FAILURE;
  }

  // Get boson 
  for(int idx=0; idx<(int)fin_Xparent->nChildren(); idx++){
    xAOD::TruthParticle* child = (xAOD::TruthParticle*) fin_Xparent->child(idx);
    if(!child) continue;
    if( isViaW  && std::abs(child->pdgId())!=24)             continue;
    if( isViaZh && child->pdgId()!=23 && child->pdgId()!=25) continue;
    boson = child;
    break;
  }

  if(!boson){
    std::cout << "BosonPolReweightingTool::GetEWGDecay  ERROR  Could not retrieve the child boson. " << std::endl;
    std::cout << "BosonPolReweightingTool::GetEWGDecay  ERROR  Parent pdg: " << Xparent->pdgId() << ", chidl_pdg: " << Xchild->pdgId() << std::endl;
    std::cout << "BosonPolReweightingTool::GetEWGDecay  ERROR  isParentC: " << isParentC << ", isParentN: " << isParentN << ", isChildC: " << isChildC << ", isChildN: " << isChildN << ". Exit." << std::endl;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
float BosonPolReweightingTool::getCosThetaStar(tlv pXparent, tlv pBoson){

  tlv pBoson_parentRF = pBoson;
  pBoson_parentRF.Boost(-pXparent.BoostVector());
  float cosThetaStar = pBoson_parentRF.Vect().Unit() * pXparent.Vect().Unit();

  if( !(fabs(cosThetaStar)<1.) ){
    std::cout << "BosonPolReweightingTool::getPolCosThetaStar  ERROR  Invalid cosThetaStar value (" << cosThetaStar << ") detected! This should be [-1,1]. beta_Xparent= " << pXparent.Beta() << ". Return -9." << std::endl;
    return -9.;
  }

  return cosThetaStar;
}

//////////////////////////////////////////////////
float BosonPolReweightingTool::getCosThetaStar(xAOD::TruthParticle *Xparent, xAOD::TruthParticle *boson){

  tlv pXparent = m_decayHandle->setTLV(Xparent);
  tlv pBoson = m_decayHandle->setTLV(boson);

  return getCosThetaStar(pXparent, pBoson);

}
//////////////////////////////////////////////////
TString BosonPolReweightingTool::getProcString(const int &Xparent_pdg, const int &Xchild_pdg, const int &boson_pdg,
					   const TString &XparentFlavor, const TString &XchildFlavor){

  TString modelString = XparentFlavor+XchildFlavor;
  modelString.ReplaceAll("hinoAwino","hinoAWino");
  modelString.ReplaceAll("hinoBwino","hinoBWino");
  modelString.ReplaceAll("winohino","winoHino");
  modelString.ReplaceAll("winobino","winoBino");
  modelString.ReplaceAll("hinoAbino","hinoABino");
  modelString.ReplaceAll("hinoBbino","hinoBBino");

  const bool isParentC = m_decayHandle->isChargino(Xparent_pdg);
  const bool isParentN = m_decayHandle->isNeutralino(Xparent_pdg);

  TString procString="";
  if(isParentC && std::abs(boson_pdg)==24){
    TString sq = boson_pdg>0 ? "p" : "m";
    procString = 
      modelString=="winoBino"  ? "C1"+sq+"_W"+sq+"_N1" :
      modelString=="hinoABino" ? "C1"+sq+"_W"+sq+"_N1" :
      modelString=="hinoAWino" ? "C2"+sq+"_W"+sq+"_N1" :
      modelString=="winoHinoA" ? "C2"+sq+"_W"+sq+"_N1" :
      modelString=="winoHinoB" ? "C2"+sq+"_W"+sq+"_N2" : "";
  }
  else if(isParentN && std::abs(boson_pdg)==24){
    TString sqC = boson_pdg>0 ? "m" : "p";
    TString sqW = boson_pdg>0 ? "p" : "m";
    procString = 
      modelString=="hinoAWino" ? "N2_W"+sqW+"_C1"+sqC :
      modelString=="hinoBWino" ? "N3_W"+sqW+"_C1"+sqC :
      modelString=="winoHinoA" ? "N3_W"+sqW+"_C1"+sqC : "";
  }
  else if(isParentC && boson_pdg==23){
    TString sqC = Xparent_pdg>0 ? "p" : "m";
    procString = "C2"+sqC+"_Z_C1"+sqC;
  }
  else if(isParentN && boson_pdg==23){
    procString = 
      modelString=="winoBino"  ? "N2_Z_N1" :
      modelString=="hinoABino" ? "N2_Z_N1" :
      modelString=="hinoBBino" ? "N3_Z_N1" :
      modelString=="hinoAWino" ? "N2_Z_N1" :
      modelString=="hinoBWino" ? "N3_Z_N1" :
      modelString=="winoHinoA" ? "N3_Z_N1" :
      modelString=="winoHinoB" ? "N3_Z_N2" : "";
  }
  else{
    std::cout << "BosonPolReweightingTool::getProcString  ERROR  Weird gaugino-boson combination. Xparent_pdg: " << Xparent_pdg << ", Xchild_pdg: " << Xchild_pdg << " boson_pdg: " << boson_pdg << ". Return empty string." << std::endl;
    return "";
  }

  modelString.ReplaceAll("HinoA","Hino").ReplaceAll("HinoB","Hino");
  modelString.ReplaceAll("hinoA","hino").ReplaceAll("hinoBW","hinoW").ReplaceAll("hinoBB","hinoB");

  procString += "_"+modelString;

  return procString;
}
//////////////////////////////////////////////////
float BosonPolReweightingTool::fetchCoefficients_fixedTanb(const TString &coeffType, const TString &proc, 
						       const float &mHeavy, const float &mLight, 
						       const int &tanb_i, const int &sgnMu){

  // Sanity check on the input tanb
  if(tanb_i!=2 && tanb_i!=5 && tanb_i!=10 && tanb_i!=30){
    std::cout << "BosonPolReweightingTool::fetchCoefficients_fixedTanb  ERROR  Input tanb (" << tanb_i << ") has invalid value! It has to be either 2/5/10/30. Return -1." << std::endl;
    return -1;
  }
    
  // Get corresponding histogram
  TString histName = coeffType+"_"+proc+"_tanb"+std::to_string(tanb_i)+"_"+(sgnMu>0 ? "posMu" : "negMu")+"_dM_intp";

  std::map<TString,TH2F*>::iterator it = m_spinMap.find(histName);	
  if(it==m_spinMap.end()){
    std::cout << "BosonPolReweightingTool::fetchCoefficients_fixedTanb  ERROR  No hist " << histName << " is found " << std::endl;
    return -999.;
  }

  // Fall back if dM is too small
  float dM = mHeavy-mLight;
  if(dM<100){
    std::cout << "BosonPolReweightingTool::fetchCoefficients_fixedTanb  WARNING  dM(" << dM << ")<100GeV is not supported. Fill dM=100GeV values as the approximation. " << std::endl;
    dM = 100.;
  }

  // Get value from histogram
  TH2F* hist = m_spinMap[histName];
  float F = hist->GetBinContent(hist->FindBin(mHeavy,dM));

  return F;

}
//////////////////////////////////////////////////
float BosonPolReweightingTool::fetchCoefficients(const TString &coeffType, const TString &proc, 
					     const float &mHeavy, const float &mLight, 
					     const float &tanb, const int &sgnMu){

  //
  // Interpolate the coeffcient along tan(beta)
  //

  if     (tanb<2.)  return fetchCoefficients_fixedTanb(coeffType,proc,mHeavy,mLight,2,sgnMu);
  else if(tanb>30.) return fetchCoefficients_fixedTanb(coeffType,proc,mHeavy,mLight,30,sgnMu);

  int tanb_low  = tanb<5. ? 2 : tanb<10. ?  5 : 10; 
  int tanb_high = tanb<5. ? 5 : tanb<10. ? 10 : 30;
  
  float F_low = fetchCoefficients_fixedTanb(coeffType,proc,mHeavy,mLight,tanb_low,sgnMu);
  float F_high = fetchCoefficients_fixedTanb(coeffType,proc,mHeavy,mLight,tanb_high,sgnMu);
  return F_low + ((F_high-F_low)/(tanb_high-tanb_low)) * (tanb-tanb_low);

}
//////////////////////////////////////////////////
void BosonPolReweightingTool::normalizeCoefficients(float &F0, float &FL, float &FR){

  const float norm = sqrt(pow(F0,2)+pow(FL,2)+pow(FR,2));
  F0 /= norm;
  FL /= norm;
  FR /= norm;

}

//////////////////////////////////////////////////
float BosonPolReweightingTool::getPolWeight(xAOD::TruthParticle *Xparent, 
                                        xAOD::TruthParticle *Xchild, 
                                        xAOD::TruthParticle *boson, 
                                        TString XparentFlavor, TString XchildFlavor, 
                                        float tanb, int sgnMu){

  TString proc = getProcString(Xparent->pdgId(), Xchild->pdgId(), boson->pdgId(), XparentFlavor, XchildFlavor);
  if(proc=="") return 1.;

  const float cosThetaStar = getCosThetaStar(Xparent,boson);
  if(fabs(cosThetaStar)>1.) return 1.;

  const float mHeavy = Xparent->m()*m_convertFromMeV;
  const float mLight =  Xchild->m()*m_convertFromMeV;

  // Ignore off-shell bosons
  if(mHeavy-mLight<75.) return 1.;

  float F0 = fetchCoefficients("F0",proc,mHeavy,mLight,tanb,sgnMu);
  float FL = fetchCoefficients("FL",proc,mHeavy,mLight,tanb,sgnMu);
  float FR = fetchCoefficients("FR",proc,mHeavy,mLight,tanb,sgnMu);
  normalizeCoefficients(F0,FL,FR);
  m_func_cosThetaStar->SetParameters(1.,F0,FL,FR);  
  m_func_cosThetaStar->SetParameter(0, 1./m_func_cosThetaStar->Integral(-1.,1.));  

  return m_func_cosThetaStar->Eval(cosThetaStar) / (1./2);  // Default cosThetaStar: flat -> f(x)=1/2 (x=[-1,1])
}

//////////////////////////////////////////////////
float BosonPolReweightingTool::getCombinedPolWeight(xAOD::TruthParticleContainer *truthParticles,
					        TString XparentFlavor, TString XchildFlavor, float tanb, int sgnMu){

  float weight = 1.;

  std::vector<EWGDecay> decays = GetEWGDecays(truthParticles);
  for(EWGDecay decay : decays)
    weight *= getPolWeight(decay.Xparent, decay.Xchild, decay.boson, XparentFlavor, XchildFlavor, tanb, sgnMu);
  

  return weight;
}

//////////////////////////////////////////////////
