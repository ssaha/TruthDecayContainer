#include "TruthDecayContainer/Decay_H.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/Decay_tau.h"
#include "TruthDecayContainer/TruthDecayUtils.h"

//////////////////////////////////////////////////////
//
// Container storing the decay information of Higgs boson
//
/////////////////////////////////////////////////////

typedef TLorentzVector tlv;

Decay_H::Decay_H() :
  q11_pdg(0),
  q12_pdg(0),
  q21_pdg(0),
  q22_pdg(0),
  decayLabel_sub1(-1),
  decayLabel_sub2(-1)
{
  clear();  
}

Decay_H::~Decay_H(){}

Decay_H::Decay_H(const Decay_H &rhs) :
  Decay_boson(rhs),
  pq11(rhs.pq11),
  pq12(rhs.pq12),
  pq21(rhs.pq21),
  pq22(rhs.pq22),
  q11_pdg(rhs.q11_pdg),
  q12_pdg(rhs.q12_pdg),
  q21_pdg(rhs.q21_pdg),
  q22_pdg(rhs.q22_pdg),
  decayLabel_sub1(rhs.decayLabel_sub1),
  decayLabel_sub2(rhs.decayLabel_sub2)
{}

///////////////////////////////////////////////////
void Decay_H::finalize(const Decay_boson   &W1,   const Decay_boson   &W2,
		       const Decay_boson   &Z1,   const Decay_boson   &Z2){

  // input : H's 4-vector (P4), H children's tlv, pdg 

  pdg = 25;

  if(std::abs(q1_pdg)==22)      
    decayLabel=0;
  else if(std::abs(q1_pdg)==5) 
    decayLabel=1;
  else if(std::abs(q1_pdg)==15) {
    decayLabel=2;
    /*
    pq11 = tau1.pchild;
    pq12 = tau1.ptaunu;
    pq21 = tau2.pchild;
    pq11 = tau2.ptaunu;
    q11_pdg = tau1.child_pdg;
    q12_pdg = -(tau1.pdg>0)*16;
    q21_pdg = tau2.child_pdg;
    q22_pdg = -(tau2.pdg>0)*16;
    decayLabel_sub1 = tau1.decayLabel;
    decayLabel_sub2 = tau2.decayLabel;
    */
    pMis = tau1.pMis + tau2.pMis;
  }
  else if(std::abs(q1_pdg)==24) {
    decayLabel=3;   
    pq11 = W1.pq1;
    pq12 = W1.pq2;
    pq21 = W2.pq1;
    pq22 = W2.pq2;
    q11_pdg = W1.q1_pdg;
    q12_pdg = W1.q2_pdg;
    q21_pdg = W2.q1_pdg;
    q22_pdg = W2.q2_pdg;
    decayLabel_sub1 = W1.decayLabel;
    decayLabel_sub2 = W2.decayLabel;
    pMis = W1.pMis + W2.pMis;
  }
  else if(std::abs(q1_pdg)==23) {
    decayLabel=4;
    pq11 = Z1.pq1;
    pq12 = Z1.pq2;
    pq21 = Z2.pq1;
    pq22 = Z2.pq2;
    q11_pdg = Z1.q1_pdg;
    q12_pdg = Z1.q2_pdg;
    q21_pdg = Z2.q1_pdg;
    q22_pdg = Z2.q2_pdg;
    decayLabel_sub1 = Z1.decayLabel;
    decayLabel_sub2 = Z2.decayLabel;
    pMis = Z1.pMis + Z2.pMis;
  }
  else if(std::abs(q1_pdg)==12 || std::abs(q1_pdg)==14 || std::abs(q1_pdg)==16) {
    pMis = P4;
    decayLabel=5;
  }
  else if(std::abs(q1_pdg)>=1 && std::abs(q1_pdg)<=3) 
    decayLabel=6;
  else if(std::abs(q1_pdg)==4) 
    decayLabel=7;
  else if(std::abs(q1_pdg)==21) 
    decayLabel=8;

  //
  check_swap();
 
  return;
}

///////////////////////////////////////////////////
TString Decay_H::getDecayString(const int &decayLabel, const int &decayLabel_sub1, const int &decayLabel_sub2){

  // yy
  if(decayLabel==0)      return "#gamma#gamma";

  // bb
  else if(decayLabel==1) return "bb";

  // tautau
  else if(decayLabel==2)
     return "#tau#tau>"
       +Decay_tau::getDecayString_tautau(decayLabel_sub1,decayLabel_sub2);   
    //      +",tau+>"+Decay_tau::getDecayString(decayLabel_sub1)
    //      +",tau->"+Decay_tau::getDecayString(decayLabel_sub2);  
  

  // WW
  else if(decayLabel==3) 
    return "WW>"
      +Decay_boson::getDecayString_W(decayLabel_sub1)
      +Decay_boson::getDecayString_W(decayLabel_sub2);
  

  // ZZ
  else if(decayLabel==4) 
    return "ZZ>"
      +Decay_boson::getDecayString_Z(decayLabel_sub1)
      +Decay_boson::getDecayString_Z(decayLabel_sub2);

  else if(decayLabel==5) return "vv";
  else if(decayLabel==6) return "uu/dd/ss";
  else if(decayLabel==7) return "cc";
  else if(decayLabel==8) return "gg";
  
  else    return "unknown";   

  return "";

}

///////////////////////////////////////////////////
void Decay_H::swap12(){

  Decay_boson::swap12();

  TruthDecayUtils::swap(pq11, pq21);
  TruthDecayUtils::swap(pq12, pq22);
  TruthDecayUtils::swap(q11_pdg, q21_pdg);
  TruthDecayUtils::swap(q21_pdg, q22_pdg);
  TruthDecayUtils::swap(decayLabel_sub1, decayLabel_sub2);

  return;
}

///////////////////////////////////////////////////
void Decay_H::check_swap(){
  if(q1_pdg>0 && q2_pdg<0) swap12(); // always set q1 as one with pdgId>0
}

///////////////////////////////////////////////////
void Decay_H::print(){


  std::cout << "  Decay_H::print() --------------------------------------" << std::endl;
  Decay_boson::print();

  std::cout << "    decayLabel: " << decayLabel
       << " " << getDecayString(decayLabel,decayLabel_sub1,decayLabel_sub2)
       <<  std::endl;
  
  TruthDecayUtils::print4mom(pq11,  "pq11");
  TruthDecayUtils::print4mom(pq12,  "pq12");
  std::cout << "    q11_pdg: " <<  q11_pdg << std::endl;
  std::cout << "    q12_pdg: " <<  q12_pdg << std::endl;
  std::cout << "    decayLabel_sub1: " <<  decayLabel_sub1 << std::endl;
  
  TruthDecayUtils::print4mom(pq21,  "pq21");
  TruthDecayUtils::print4mom(pq22,  "pq22");
  std::cout << "    q21_pdg: " <<  q21_pdg << std::endl;
  std::cout << "    q22_pdg: " <<  q22_pdg << std::endl;
  std::cout << "    decayLabel_sub2: " <<  decayLabel_sub2 << std::endl;
  

  std::cout << "  --------------------------------------------------------" << std::endl;


  return;
}

///////////////////////////////////////////////////
void Decay_H::clear(){

  Decay_boson::clear();

  // tlv
  pq11.SetXYZM(0,0,0,0); pq12.SetXYZM(0,0,0,0);  
  pq21.SetXYZM(0,0,0,0); pq22.SetXYZM(0,0,0,0);  

  // decay labels
  decayLabel_sub1=-1;
  decayLabel_sub2=-1;

  // pdg
  q11_pdg=0; q12_pdg=0;
  q21_pdg=0; q22_pdg=0;

  
  return;
}
///////////////////////////////////////////////////
